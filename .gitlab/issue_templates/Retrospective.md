> Regardless of what we discover today, we understand and truly believe that everyone did the best job they could, given what they knew at the time, their skills and abilities, the resources available, and the situation at hand.

## Discussion categories

- Enjoyable
- Frustrating
- Puzzling
- Same
- More
- Less


## Mute Mapping

| Topic | Related comment(s) |
| ------ | ------ |
|   Related topic A     |  ...corresponding comment summaries     |
|   Related topic B     |  ...corresponding comment summaries      |

## Action Items

1. Create a new discussion thread called **Action Items**
2. Discuss what to do more or suggest process improvements based on the mute mapping exercise. 
3. Capture agreed-upon action items as the next steps below. Ensure each next step has a corresponding "Directly Responsible Individual (DRI)" for following through on the action item. 

Next steps:

- [ ] ...
- [ ] ...