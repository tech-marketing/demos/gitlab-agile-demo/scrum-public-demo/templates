Sprint planning board: https://gitlab.com/groups/tech-marketing/demos/gitlab-agile-demo/scrum-public-demo/-/boards/7302477

Team: `@gweaver` `@mushakov` `@davistye` (uncomment these to automatically create a todo for each team member) 

Velocity: `X`

Discussion template:

```
## [Link to issue] 

- [ ] Acceptance criteria defined
- [ ] Weight set
- [ ] Implementation steps (tasks) created
```

**Tip:** Append `+` to the issue URL to automatically unfurl the title. Append `+s` to the issue title to automatically unfurl the title, milestone, and assignees.