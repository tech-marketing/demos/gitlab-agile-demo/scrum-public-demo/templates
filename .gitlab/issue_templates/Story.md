## Story

> When [insert the user's situation], I want to [insert the user's motivation], so I can [insert the user's expected outcome]

## UX

- [ ] Add relevant designs to this story's [design manager](https://docs.gitlab.com/ee/user/project/issues/design_management.html)

## Acceptance Criteria

The following acceptance criteria need to be in order to complete this story:

- [ ] ...
- [ ] 

/label ~type::story ~status::refine