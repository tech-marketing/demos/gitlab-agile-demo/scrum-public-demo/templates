Daily thread template:

```
## [Insert today's date] Standup

@mention each team member
````

Thread comment template:

```
## What I accomplished yesterday

## What I'm working on today

## Blockers
```